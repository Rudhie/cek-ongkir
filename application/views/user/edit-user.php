<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-users"></i> Ubah Master Data User</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('#'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('user'); ?>">Data User</a></li>
            <li class="active"><strong>Ubah User</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                    <div class="row">
                        <form action="<?php echo base_url('user/action_ubah/').$this->input->get('id'); ?>" method="POST" role="form">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Nama User</label>
                                    <input type="text" name="nama_user" value="<?php echo $user[0]->nama_user; ?>" class="form-control" placeholder="Nama User Baru" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" placeholder="Email" name="email" value="<?php echo $user[0]->email; ?>" required="">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="New Password" name="password" value=""required="">
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group">
                                <label>Role</label>
                                <select class="form-control m-b" id="selectRole" name="role" required>
                                    <option>Pilih SRole</option>
                                    <option value="admin" <?php if($user[0]->role == "admin"){echo "selected";} ?>>Admin</option>
                                    <option value="user" <?php if($user[0]->role == "user"){echo "selected";} ?>>User</option>
                                </select>
                                </div>
                            </div>
                           <!--  <div class="col-md-8">
                                <div class="form-group">
                                    <label>Nip</label>
                                    <input type="text" class="form-control" name="nip" value="<?php echo $karyawan[0]->nip; ?>" placeholder="Nip Baru">
                                </div>
                            </div> -->
                            <div class="col-md-12">
                                <div class="form-group">
                                <label>Status</label>
                                <select class="form-control m-b" id="selectUser" name="status" required>
                                    <option>Pilih Status</option>
                                    <option value="aktif" <?php if($user[0]->status == "aktif"){echo "selected";} ?>>Aktif</option>
                                    <option value="nonaktif" <?php if($user[0]->status == "nonaktif"){echo "selected";} ?>>Nonaktif</option>
                                </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                    <a href="<?php echo base_url('user') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
    
    });
</script>