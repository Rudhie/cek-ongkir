<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-users"></i> Master Data User</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data User</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role')=='admin'){ ?>
                    <a href="<?php echo base_url('user/tambah'); ?>" class="btn btn-outline btn-info dim"><i class="fa fa-plus"></i> Tambah</a>
                </div> <?php } ?>
                   
                <div class="ibox-content">
                <?php if($this->session->flashdata("success")){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("success"));
                            unset($_SESSION["success"]);
                        ?>
                    </div>
                <?php } else if($this->session->flashdata("error")) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("error"));
                            unset($_SESSION["error"]);
                        ?>
                    </div>
                <?php } ?>
                    <div class="table-responsive">
                    <table id="tableUser" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Nama User</th>
                        <th>Email</th>
                        <!--<th>Password</th>-->
                        <th>Role</th>
						<!-- <th>nip</th> -->
                        <th>status</th>
                        <?php if($this->session->userdata('role')=='admin'){?>

                        <th width="15%">Aksi</th>
                    <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                 
                   <?php $i=1;
                   foreach($user as $p){
                    echo'<tr >';
                    echo'<td>'.$i.'</td>';
                    echo'<td>'.$p->nama_user.'</td>';
                    echo '<td>'.$p->email.'</td>';
                    echo'<td>'.$p->role.'</td>';
					 // echo'<td>'.$p->nip.'</td>';
                    echo'<td>'.$p->status.'</td>';
                    if($this->session->userdata('role') == 'admin'){
                        echo'<td><a href="'.base_url('user/edit').'?id='.$p->id_user.'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> UBAH</a> '.'<a onclick="hapusUser('.$p->id_user.')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> HAPUS</a> </td>';
                    }
                    echo'</tr>';
                    $i++;
                   }
                   ?>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        $("#tableUser").dataTable();
    });

    function hapusUser(id){
        var hapus = confirm("Apakah anda yakin ingin menghapus?");
        if(hapus){
            window.location = "<?php echo base_url('user/action_hapus/') ?>"+id;
        }
    }
</script>