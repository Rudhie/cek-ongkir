<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-users"></i> Tambah Master Data User</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('user'); ?>">Data User</a></li>
            <li class="active"><strong>Tambah User</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                    <div class="row">
                        <form action="<?php echo base_url('user/action_tambah'); ?>" method="POST" role="form">
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Nama User</label>
                                    <input type="text" name="nama_user" class="form-control" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" id="pw1" name="password" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Konfirmasi Password</label>
                                    <input type="password" class="form-control" id="pw2" name="password" required="">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="form-control m-b" id=selectRole name="role" required>
                                        <option>Pilih Role</option>
                                        <option value="admin">Admin</option>
                                        <option value="user">User</option>
                                    </select>
                                    <!-- <input type="text" class="form-control" name="jabatan" placeholder="Role User Baru"> -->
                                </div>
                            </div>
                        </div>
							<!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nip</label>
                                    <input type="text" class="form-control" name="nip" placeholder="Nip Baru">
                                </div>
                            </div> -->
							<!--<div class="col-md-12">
                                <div class="form-group">
                                    <label>Status</label>
                                    <textarea class="form-control" name="status" rows="3" placeholder="Status"></textarea>
                                </div>-->
                           <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">
                                <label>Status</label>
                                <select class="form-control m-b" id="selectUser" name="status" required>
                                    <option>Pilih Status</option>
                                    <option value="aktif">Aktif</option>
                                    <option value="nonaktif">Nonaktif</option>
                                </select>
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                    <a href="<?php echo base_url('user') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
    
    });
</script>

<script type="text/javascript">
    window.onload = function () {
    document.getElementById("pw1").onchange = validatePassword;
    document.getElementById("pw2").onchange = validatePassword;
                                    }
  function validatePassword(){
    var pass2=document.getElementById("pw2").value;
    var pass1=document.getElementById("pw1").value;
        if(pass1!=pass2)
            document.getElementById("pw2").setCustomValidity("Passwords Tidak Sama, Coba Lagi");
        else
            document.getElementById("pw2").setCustomValidity('');
        }
        
</script>