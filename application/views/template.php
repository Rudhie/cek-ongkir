<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome to <?php echo $this->config->item('site_name'); ?></title>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/images/ofc.png') ?>" />
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/animate.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/plugins/datepicker/datepicker3.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/ico.png" />
    <!-- Mainly scripts -->
    <script src="<?php echo base_url('assets/js/jquery-2.1.1.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/metisMenu/jquery.metisMenu.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/slimscroll/jquery.slimscroll.min.js'); ?>"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url('assets/js/inspinia.js');?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/pace/pace.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/morris.min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/morris.js');?>"></script>
    <script src="<?php echo base_url('assets/js/raphael-min.js');?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/rickshaw/vendor/d3.v3.js');?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/rickshaw/rickshaw.min.js');?>"></script>


    <!-- Datepicker -->
    <script src="<?php echo base_url('assets/js/plugins/datepicker/bootstrap-datepicker.js') ?>"></script>

    <!-- Datatables -->
    <link href="<?php echo base_url('assets/js/morris.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/plugins/dataTables/datatables.min.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/js/plugins/dataTables/datatables.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/dataTables/dataTables.fixedColumns.min.js') ?>"></script>

    <!-- Highchart -->
    <script src="<?php echo base_url('assets/Highcharts-7.1.1/code/highcharts.js');?>"></script>

    <!-- Select2 -->
    <link href="<?php echo base_url('assets/css/plugins/select2/select2.min.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js') ?>"></script>

    <!-- Icheck -->
    <link href="<?php echo base_url('assets/css/plugins/iCheck/custom.css') ?>" rel="stylesheet">
    <script src="<?php echo base_url('assets/js/plugins/iCheck/icheck.min.js') ?>"></script>

    <!-- Progress Wizard -->
    <link href="<?php echo base_url('assets/css/progress-wizard.min.css') ?>" rel="stylesheet">

    <!-- Input Mask-->
    <!-- <script src="<?php echo base_url('assets/js/plugins/Inputmask-2.x/dist/jquery.inputmask.bundle.min.js') ?>"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
</head>
<body class="">
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <span>
                        <center>
                            <img alt="image" class="img-circle" src="<?php echo base_url('assets/images/profil.jpg')?>" />
                         </center>
                         </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                            <center>
                                <strong class="font-bold"><?php echo strtoupper($this->session->userdata("nama_user")); ?></strong>
                            </center>
                                </span>
                                <center>
                                    <span class="text-muted text-xs block"><?php echo strtoupper($this->session->userdata("role")); ?> <b class="caret"></b>
                                    </span>
                                </center>
                            </span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="<?php echo base_url("user1"); ?>">Setting</a></li>
                            <li><a href="<?php echo base_url("login/doLogout"); ?>">Keluar</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        PKBU
                    </div>
                </li>
                <?php $menu = $this->uri->segment('1'); $submenu = $this->uri->segment('2');?>
                <li <?php if($menu == "dashboard"){ echo "class='active'";} ?>>
                    <a href="#"><i class="fa fa-th-large"></i><span class="nav-label">Dashboard</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                    	<li <?php if($submenu == "dashboard1"){ echo "class='active'";} ?>>
                            <a href="<?php echo base_url("#"); ?>">Dashboard 1</a>
                        </li>
                        <li <?php if($submenu == "dashboard2"){ echo "class='active'";} ?>>
                            <a href="<?php echo base_url("#"); ?>">Dashboard 2</a>
                        </li>
                    </ul>
                </li>
                <li <?php if($menu == "kantorinduk" || $menu == "subinduk"){ echo "class='active'";} ?>>
                    <a href="#"><i class="fa fa-book"></i> <span class="nav-label">Pencapaian Pembina</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li <?php if($menu == "kantorinduk"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("kantorinduk?tahun=".date('Y')); ?>"><i class="fa fa-square"></i>Iuran</a>
                        </li>
                        <li <?php if($menu == "subinduk"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("subinduk?bulan=".date('n')."&tahun=".date('Y').""); ?>"><i class="fa fa-square"></i>Penambahan</a>
                        </li>

                    </ul>
                </li>
                
                        <li <?php if($menu == "itw"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("itw?tahun=".date('Y')); ?>"><i class="fa fa-check-square"></i> <span class="nav-label">Tepat Waktu</span></a>
                        </li>
                <li <?php if($menu == "kantorperintis" || $menu == "subperintis"){ echo "class='active'";} ?>>
                    <a href="#"><i class="fa fa-file-text-o"></i> <span class="nav-label">Pencapaian Cabang</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li <?php if($menu == "kantorperintis"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("kantorperintis?tahun=".date('Y')); ?>"><i class="fa fa-square-o"></i>Iuran</a>
                        </li>
                        <li <?php if($menu == "subperintis"){ echo "class='active'";} ?>>
                        <a href="<?php echo base_url("subperintis?bulan=".date('n')."&tahun=".date('Y').""); ?>"><i class="fa fa-square-o"></i>Penambahan</a>
                        </li>
                    </ul>
                </li>
                    
                <li <?php if($menu == "user" || $menu == "user" || $menu == "kantor" || $menu == "pembina"){ echo "class='active'";} ?>>
                    <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Master</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li <?php if($menu == "kantor"){ echo "class='active'";} ?>><a href="<?php echo base_url("kantor"); ?>"><i class="fa fa-building"></i>Kantor</a></li>
                        <li <?php if($menu == "pembina"){ echo "class='active'";} ?>><a href="<?php echo base_url("pembina"); ?>"><i class="fa fa-user"></i>Pembina</a></li>
                        <?php if($this->session->userdata('role')=='admin'){ ?>
                        <li <?php if($menu == "user" && $submenu == null){ echo "class='active'";} ?>><a href="<?php echo base_url("user"); ?>"><i class="fa fa-users"></i>User Management</a></li>
                        <?php } ?>
                    </ul>
                </li>

                <li <?php if($menu == "pkbu" || $menu =="actplan" || $menu == "kunjungan"){ echo "class='active'";} ?>>
                    <a href="#"><i class="fa fa-list"></i> <span class="nav-label">PKBU</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li <?php if($menu == "actplan"){ echo "class='active'";} ?>><a href="<?php echo base_url("actplan?startdate=".date('d-m-Y', strtotime('-1 month'))."&enddate=".date('d-m-Y')."&status=all".""); ?>"><i class="fa fa-calendar"></i>Action Plan</a></li>
                        <li <?php if($menu == "kunjungan"){ echo "class='active'";} ?>><a href="<?php echo base_url("kunjungan?nm_pkbu=all".""); ?>"><i class="fa fa-edit"></i>Kunjungan</a></li>
                        <li <?php if($menu == "pkbu"){ echo "class='active'";} ?>><a href="<?php echo base_url("pkbu"); ?>"><i class="fa fa-credit-card"></i>PKBU</a></li>
                        
                    </ul>
                </li>

                <li <?php if($menu == "perisai"){ echo "class='active'";} ?>>
                    <a href="<?php echo base_url("perisai"); ?>"><i class="fa fa-shield"></i> <span class="nav-label">Perisai</span></a>
                </li>

                <li <?php if($menu == "kehadiran"){ echo "class='active'";} ?>>
                    <a href="<?php echo base_url("kehadiran"); ?>"><i class="fa fa-check-square-o"></i> <span class="nav-label">Kehadiran</span></a>
                </li>
            </ul>
        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0;background: linear-gradient(to right, #627699 0%, #909eb8 100%);color:black;">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-success dim " href="#"><i class="fa fa-bars"></i> </a>
        </div>
            <ul class="nav navbar-top-links navbar-right">

                <li>
                    <a class="dropdown-toggle count-info" href="<?php echo base_url("ultah"); ?>">
                        <i class="fa fa-birthday-cake" data-toggle="tooltip" data-placement="bottom" title="Tanggal Ultah"></i> <span class="label label-primary"></span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url("login/doLogout"); ?>">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </nav>
        </div>
            <!-- content in here -->
            <?= $contents; ?>
            <!-- /content in here -->

            <div class="footer">
                <div class="pull-right">
                    Simonster &copy; <?php echo date('Y'); ?>
                </div>
            </div>

        </div>
    </div>
</body>
</html>