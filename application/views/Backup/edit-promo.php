<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa shopping-chart"></i> Ubah Master Data User</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('promo'); ?>">Data User</a></li>
            <li class="active"><strong>Ubah User</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                    <div class="row">
                        <form action="<?php echo base_url('promo/action_ubah/').$this->input->get('id'); ?>" method="POST" role="form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama akun</label>
                                    <input type="text" name="nama_akun" value="<?php echo $promo[0]->nama_akun; ?>" class="form-control" placeholder="Nama Akun Baru" required>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Nama Promo</label>
                                    <input type="text" class="form-control" placeholder="Nama Promo" name="nama_promo" value="<?php echo $promo[0]->nama_promo; ?>" required="">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group" id="data_1">
                                    <label>Tgl Sell Out Total</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" placeholder="New Date" name="tgl_sell_out_total" value="<?php echo date('d/m/Y', strtotime($promo[0]->tgl_sell_out_total));?>" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>SKU</label>
                                    <input type="text" class="form-control" name="sku_total" value="<?php echo $promo[0]->sku_total; ?>" placeholder="New SKU">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Jumlah</label>
                                    <input type="text" class="form-control" name="quantity_total" value="<?php echo $promo[0]->quantity_total; ?>" placeholder="New Quantity">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price_total" value="<?php echo str_replace(",", ".",  number_format($promo[0]->price_total)); ?>" placeholder="New price">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group" id="data_1">
                                    <label>Tgl Sell Out Promo</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" placeholder="New Date" name="tgl_sell_out_onpromo" value="<?php echo date('d/m/Y', strtotime($promo[0]->tgl_sell_out_onpromo));?>" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>SKU</label>
                                    <input type="text" class="form-control" name="sku" value="<?php echo $promo[0]->sku; ?>" placeholder="New SKU">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Jumlah</label>
                                    <input type="text" class="form-control" name="quantity" value="<?php echo $promo[0]->quantity; ?>" placeholder="New Quantity">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="text" class="form-control" name="price" value="<?php echo $promo[0]->price; ?>" placeholder="New price">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group" id="data_1">
                                    <label>Promo Start</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" placeholder="New Date" name="tgl_durasi_start" value="<?php echo date('d/m/Y', strtotime($promo[0]->tgl_durasi_start));?>" required="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group" id="data_1">
                                    <label>Promo End</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" class="form-control" placeholder="New Date" name="tgl_durasi_end" value="<?php echo date('d/m/Y', strtotime($promo[0]->tgl_durasi_end));?>" required="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Biaya Fixed</label>
                                    <input type="text" class="form-control" name="biaya_fixed" id="inputmasking1" value="<?php echo $promo[0]->biaya_fixed; ?>" placeholder="New price">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Biaya Variable</label>
                                    <input type="text" class="form-control" name="biaya_variable" value="<?php echo $promo[0]->biaya_variable; ?>" placeholder="New price">
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>GP<small> (%) </small></label>
                                    <input type="text" class="form-control" name="gp" value="<?php echo $promo[0]->gp; ?>" placeholder="New precentage">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Target Budget Tahunan</label>
                                    <input type="text" class="form-control" name="target_budget_tahunan" value="<?php echo $promo[0]->target_budget_tahunan; ?>" placeholder="New Budget">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Budget Terpakai</label>
                                    <input type="text" class="form-control" name="budget_terpakai" value="<?php echo $promo[0]->budget_terpakai; ?>" placeholder="New Budget">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Total Budget Terpakai</label>
                                    <input type="text" class="form-control" name="total_budget_terpakai" value="<?php echo $promo[0]->total_budget_terpakai; ?>" placeholder="New Budget">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Margin</label>
                                    <input type="text" class="form-control" name="margin" value="<?php echo $promo[0]->margin; ?>" placeholder="New Margin">
                                </div>
                            </div>
                        </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                    <a href="<?php echo base_url('promo') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                </div>
                            </div>

                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'

        });
        // $("#inputmasking1, #inputmasking2, #inputmasking3, #inputmasking4, #inputmasking5, #inputmasking6, #inputmasking7, #inputmasking8").mask("000.000.000.000", {reverse: true});
        // $("#percentmasking").mask("##0,00", {reverse: true});
    });
</script>