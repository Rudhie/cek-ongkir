<style type="text/css">
    #tdInvoice {
        border-bottom: 1px solid #DDDDDD;
        text-align: right;
        width: 15%;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-paper-plane"></i> Detail Promo</h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('promo'); ?>">Data Promo</a></li>
            <li class="active"><strong><a>Detail Promo</a></strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <h1>
                        <i class="fa fa-university"></i> <?php echo $promo[0]->nama_akun; ?>
                        <a style="float: right;" href="<?php echo base_url('promo') ?>" class="btn btn-sm btn-primary"><i class="fa fa-arrow-left"></i> KEMBALI</a>
                    </h1>
                    <hr style="border-top: dotted 2px;"/>
                    <div class="row">
                        <div class="col-md-4">
                            <?php
                                function tanggal_indo($tanggal){
                                    $bulan = array(1 => 'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                                    $split = explode('-', $tanggal);
                                    return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
                                }
                            ?>
							 <blockquote>
                                <p><i class="fa fa-university"></i> Nama Promo </p>
                                <small><strong><?php echo strtoupper($promo[0]->nama_promo); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-calendar"></i> Tanggal Sell Out Total </p>
                                <small><strong><?php echo tanggal_indo($promo[0]->tgl_sell_out_total); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-barcode"></i>SKU</p>
                                <small><strong><?php echo sku($promo[0]->sku); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-calculator"></i> Jumlah </p>
                                <small><strong><?php echo quantity($promo[0]->quantity); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-money"></i> Harga </p>
                                <samll><strong><?php echo price($promo[0]->price); ?></strong></samll>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-calendar"></i> Durasi Start </p>
                                <small><strong><?php echo tanggal_indo($promo[0]->tgl_durasi_start); ?></strong></small>
                            </blockquote>
                            <blockquote>
                                <p><i class="fa fa-calendar"></i> Durasi End</p>
                                <small><strong><?php echo tanggal_indo($promo[0]->tgl_durasi_end); ?></strong></small>
                            </blockquote>
                        </div>
                        <div class="col-md-8">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Daftar Budget</div>
                                <div class="panel-body">
                                    <table class="table invoice-table">
                                        <thead>
                                            <tr>
                                                <th>Total Budget Tahunan</th>
                                                <th>Budget Terpakai</th>
                                                <th>Total Budget Terpakai</th>
                                            </tr>
                                        </thead>
                                        <tbody style="text-align: right;">
                                            <?php $total_budget_tahunan = 0; $budget_terpakai = 0; foreach($promo as $key => $value){ ?>
                                                <tr>
                                                    <td><?php echo str_replace(",", ".", number_format($value->total_budget_tahunan)); ?></td>
                                                    <td><?php echo $value->budget_terpakai; ?></td>
													 <td><?php echo $value->total_budget_terpakai; ?></td>
                                                </tr>
                                            <?php }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>