<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-shopping-cart"></i> Master Promo </h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Promo</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role')=='admin'){ ?>
                    <a href="<?php echo base_url('promo/tambah'); ?>" class="btn btn-xs btn-info"><i class="fa fa-plus"></i>Tambah Promo</a>
                </div> <?php } ?>
               <div class="ibox-content">
                    <?php if($this->session->userdata('role')=='admin'){ ?>
                    <form method="post" action="<?php echo base_url("Promex/form"); ?>" enctype="multipart/form-data">
                    <input type="file" name="file">   
                    
                    <input type="submit" name="preview" value="Preview"> 
                    </form>
                </div><?php } ?>
                    <!-- Buat sebuah tag form dan arahkan action nya ke controller ini lagi -->
                    <!-- <form method="post" action="<?php echo base_url("Siswa/form"); ?>" enctype="multipart/form-data"> -->
                        <!--
                        -- Buat sebuah input type file
                        -- class pull-left berfungsi agar file input berada di sebelah kiri
                        -->
                        <!-- <input type="file" name="file"> -->

                        <!--
                        -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
                        -->
                        <!-- <input type="submit" name="preview" value="Preview"> -->
                    <!-- </form> -->
                <div class="ibox-content">
                <?php if($this->session->flashdata("success")){ ?>
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("success"));
                            unset($_SESSION["success"]);
                        ?>
                    </div>
                <?php } else if($this->session->flashdata("error")) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                        <?php
                            echo strtoupper($this->session->flashdata("error"));
                            unset($_SESSION["error"]);
                        ?>
                    </div>
                <?php } ?>
                    <div class="table-responsive">
                    <table id="tablePromo" class="table table-striped table-bordered table-hover dataTables-example" >
                    <thead>
                        <tr>
                        <th width="5%">No</th>
                        <th>Nama Akun</th>
                        <th>Nama Promo</th>
                        <th>Sell Out Total</th>
                        <th>SKU</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Sell Out Promo</th>
                        <th>SKU</th>
                        <th>Jumlah</th>
                        <th>Harga</th>
                        <th>Durasi Start</th>
                        <th>Durasi End</th>
                        <th>Biaya Fixed</th>
                        <th>Biaya Variable</th>
                        <th>GP</th>
                        <th>T Budget Tahunan</th>
                        <th>B. Terpakai</th>
                        <th>Total B. Terpakai</th>
                        <th>Margin</th>
                        <?php if($this->session->userdata('role')=='admin'){?>

                        <th width="15%">Aksi</th>
                    <?php } ?>
                    </tr>
                   </thead>
                    <tbody>
                 
                   <?php $i=1;
                   foreach($promo as $p){
                    echo'<tr >';
                    echo'<td>'.$i.'</td>';
                    echo'<td>'.$p->nama_akun.'</td>';
                    echo'<td>'.$p->nama_promo.'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->tgl_sell_out_total)).'</td>';
                    echo'<td>'.$p->sku_total.'</td>';
                    echo'<td>'.$p->quantity_total.'</td>';
                    echo'<td> Rp.'.number_format($p->price_total,0).'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->tgl_sell_out_onpromo)).'</td>';
                    echo'<td>'.$p->sku.'</td>';
                    echo'<td>'.number_format($p->quantity,0).'</td>';
                    echo'<td> Rp.'.number_format($p->price,0).'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->tgl_durasi_start)).'</td>';
                    echo'<td>'.date('d/m/Y', strtotime($p->tgl_durasi_end)).'</td>';
                    echo'<td> Rp.'.number_format($p->biaya_fixed,0).'</td>';
                    echo'<td> Rp.'.number_format($p->biaya_variable,0).'</td>';
                    echo'<td>'.number_format($p->gp,0).' %</td>';
                    echo'<td> Rp.'.number_format($p->target_budget_tahunan,0).'</td>';
                    echo'<td> Rp.'.number_format($p->budget_terpakai,0).'</td>';
                    echo'<td> Rp.'.number_format($p->total_budget_terpakai,0).'</td>';
                    echo'<td> Rp.'.number_format($p->margin).'</td>';
                    if($this->session->userdata('role') == 'admin'){
                        echo'<td><a href="'.base_url('promo/edit').'?id='.$p->id_promo.'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> UBAH</a> '.'<a onclick="hapusPromo('.$p->id_promo.')" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> HAPUS</a> </td>';
                    }
                    echo'</tr>';
                    $i++;
                   }
                   ?>
                    </tbody>
                    </table>
                        </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function(){
        $("#tablePromo").dataTable();
    });

    function hapusPromo(id){
        var hapus = confirm("Apakah anda yakin ingin menghapus?");
        if(hapus){
            window.location = "<?php echo base_url('promo/action_hapus/') ?>"+id;
        }
    }
</script>