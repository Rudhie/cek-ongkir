<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-shopping-cart"></i> Master Pembelian </h2>
        <ol class="breadcrumb">
            <li><a  href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li class="active"><strong><a>Data Pembelian</a></strong></li>
        </ol>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <?php if($this->session->userdata('role')=='admin'){ ?>
                        <form method="post" action="<?php echo base_url("Pembelian/import"); ?>" enctype="multipart/form-data">
                            <!-- 
                            -- Buat sebuah input type file
                            -- class pull-left berfungsi agar file input berada di sebelah kiri
                            -->
                            <input type="file" name="import_data">
                            
                            <!--
                            -- BUat sebuah tombol submit untuk melakukan preview terlebih dahulu data yang akan di import
                            -->
                            <br>
                            <br>

                            <input type="submit" name="preview" value="Preview">
                        </form>
                                <?php
                                    if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form 
                                        if(isset($upload_error)){ // Jika proses upload gagal
                                            echo "<div style='color: red;'>".$upload_error."</div>"; // Muncul pesan error upload
                                            die; // stop skrip
                                        }
                                        
                                        // Buat sebuah tag form untuk proses import data ke database
                                        echo "<form method='post' action='".base_url("Pembelian/import_data")."'>";
                                        
                                        // Buat sebuah div untuk alert validasi kosong
                                        
                                        echo "<div class='table-responsive'>";
                                        echo "<table id='tablePembelian' class='table table-striped table-bordered table-hover dataTables-example'>
                                        <tr>
                                            <th colspan='25'>Preview Data</th>
                                        </tr>
                                        <tr>
                                            <th>SKU Pembelian</th>
                                            <th>Kode Satu</th>
                                            <th>Kode Dua</th>
                                            <th>Invoice Type</th>
                                            <th>Invoice Number</th>
                                            <th>Invoice Date</th>
                                            <th>Description</th>
                                            <th>Quantity</th>
                                            <th>Item Unit</th>
                                            <th>Amount</th>
                                            <th>Item Description</th>
                                            <th>Vendor Name</th>
                                        </tr>";

                                        
                                        $numrow = 1;
                                        $kosong = 0;
                                        
                                        // Lakukan perulangan dari data yang ada di excel
                                        // $sheet adalah variabel yang dikirim dari controller
                                        foreach($sheet as $row){ 
                                            // Ambil data pada excel sesuai Kolom
                                            $sku_pembelian = $row['A']; // Ambil data NIS
                                            $kode_satu = $row['B']; // Ambil data nama
                                            $kode_dua = $row['C']; // Ambil data jenis kelamin
                                            $invoice_type = $row['D']; // Ambil data alamat
                                            $invoice_no = $row['E'];
                                            $invoice_date = $row['F'];
                                            $description = $row['G'];
                                            $quantity_pembelian = $row['H'];
                                            $item_unit = $row['I'];
                                            $amount = $row['J'];
                                            $item_description = $row['K'];
                                            $vendor_name = $row['L'];
                                            
                                            // Cek jika semua data tidak diisi
                                            if($sku_pembelian == "" && $kode_satu == "" && $kode_dua == "" && $invoice_type == "" && $invoice_no == "" && $invoice_date == "" && $description == "" && $quantity_pembelian == "" && $item_unit == "" && $ammount == "" && $item_description == "" && $vendor_name == "")
                                                continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)
                                            
                                            // Cek $numrow apakah lebih dari 1
                                            // Artinya karena baris pertama adalah nama-nama kolom
                                            // Jadi dilewat saja, tidak usah diimport
                                            if($numrow > 1){
                                                // Validasi apakah semua data telah diisi
                                                $sku_pembelian_td = ( ! empty($sku_pembelian))? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
                                                $kode_satu_td = ( ! empty($kode_satu))? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
                                                $kode_dua_td = ( ! empty($kode_dua))? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
                                                $invoice_type_td = ( ! empty($invoice_type))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                                                $invoice_no_td = ( ! empty($invoice_no))? "" : " style='background: #E07171;'"; 
                                                $invoice_date_td = ( ! empty($invoice_date))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                                                $description_td = ( ! empty($description))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                                                $quantity_pembelian_td = ( ! empty($quantity_pembelian))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                                                $item_unit_td = ( ! empty($item_unit))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                                                $amount_td = ( ! empty($amount))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                                                $item_description_td = ( ! empty($item_description))? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah
                                                $vendor_name_td = ( ! empty($vendor_name))? "" : " style='background: #E07171;'"; // Jika Alamat 
                                                
                                                // Jika salah satu data ada yang kosong
                                                if($sku_pembelian == "" or $kode_satu == "" or
                                                    $kode_dua == "" or $invoice_type == ""or $invoice_no == ""or $invoice_date == ""or $description == ""or $quantity_pembelian == ""or $item_unit == ""or $amount == ""or $item_description == ""or $vendor_name == ""){
                                                    $kosong++; // Tambah 1 variabel $kosong
                                                }
                                                
                                                echo "<tr>";
                                                echo "<td".$sku_pembelian_td.">".$sku_pembelian."</td>";
                                                echo "<td".$kode_satu_td.">".$kode_satu."</td>";
                                                echo "<td".$kode_dua_td.">".$kode_dua."</td>";
                                                echo "<td".$invoice_type_td.">".$invoice_type."</td>";
                                                echo "<td".$invoice_no_td.">".$invoice_no."</td>";
                                                echo "<td".$invoice_date_td.">".$invoice_date."</td>";
                                                echo "<td".$description_td.">".$description."</td>";
                                                echo "<td".$quantity_pembelian_td.">".$quantity_pembelian."</td>";
                                                echo "<td".$item_unit_td.">".$item_unit."</td>";
                                                echo "<td".$amount_td.">".$amount."</td>";
                                                echo "<td".$item_description_td.">".$item_description."</td>";
                                                echo "<td".$vendor_name_td.">".$vendor_name."</td>";
                                                echo "</tr>";
                                            }
                                            
                                            $numrow++; // Tambah 1 setiap kali looping
                                        }
                                        
                                        echo "</table>";
                                        echo "</div>";
                                        
                                        // Cek apakah variabel kosong lebih dari 0
                                        // Jika lebih dari 0, berarti ada data yang masih kosong
                                        if($kosong > 0){
                                        ?>  
                                            <script>
                                            $(document).ready(function(){
                                                // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                                                $("#jumlah_kosong").html('<?php echo $kosong; ?>');
                                                
                                                $("#kosong").show(); // Munculkan alert validasi kosong
                                            });
                                            </script>
                                        <?php
                                        }else{ // Jika semua data sudah diisi
                                            echo "<hr>";
                                            
                                            // Buat sebuah tombol untuk mengimport data ke database
                                            echo "<button type='submit' name='import'>Import</button>";
                                            echo "<a href='".base_url("Promo")."'>Cancel</a>";
                                        }
                                        
                                        echo "</form>";
                                    }
                                    ?>
                </div> <?php } ?>
                    <script>
                        $(document).ready(function(){
                            $("#tablePembelian").dataTable();
                        });
                        }
                    </script>
                  </div>
                </div>
            </div>
        </div>
    </div>