<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><i class="fa fa-user"></i>Master Data Penjualan (Tambah)</h2>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url('dashboard'); ?>">Dashboard</a></li>
            <li><a href="<?php echo base_url('promo'); ?>">Data Penjualan</a></li>
            <li class="active"><strong>Tambah Promo</strong></li>
        </ol>
    </div>
</div>

<div class="wrapper wrapper-content animated fadeIn">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="jumbotron">
                        <div class="row">
                            <form action="<?php echo base_url('promo/action_tambah'); ?>" method="POST" role="form">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>Nama Akun</label>
                                    <input type="text" name="nama_akun" class="form-control" placeholder="Account Name" required="">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                            <label>Nama Promo</label>
                                            <input type="text" class="form-control" placeholder="Promo Name" name="nama_promo" required="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group" id="data_1">
                                    <label>Tgl Sell Out Total</label>
                                        <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" class="form-control" placeholder="Tanggal Total Penjualan" name="tgl_sell_out_total" value="<?php echo date('d/m/Y')?>" required="">
                                        </div>
                                    </div>
                                </div>
                        
                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>SKU</label>
                                            <input type="text" class="form-control" placeholder="SKU" name="sku_total" required="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>Jumlah</label>
                                            <input type="text" class="form-control" placeholder="Quantity" name="quantity_total" required="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>Price<small>(Rp.)</small></label>
                                            <input type="text" class="form-control" placeholder="Price" name="price_total" required="">
                                    </div>
                                </div>
                                    
                                <div class="col-md-3" align="left">
                                    <div class="form-group" id="data_1">
                                    <label>Tgl Sell Out Promo</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control" placeholder="Sell Out On Promo Date" name="tgl_sell_out_onpromo" value="<?php echo date('d/m/Y')?>" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>SKU</label>
                                            <input type="text" class="form-control" placeholder="SKU" name="sku" required="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>Jumlah</label>
                                            <input type="text" class="form-control" placeholder="Quantity" name="quantity" required="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>Price<small>(Rp.)</small></label>
                                            <input type="text" class="form-control" placeholder="Price" name="price" required="">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group" id="data_1">
                                    <label>Promo Start</label>
                                        <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" class="form-control" placeholder="Durasi Start" name="tgl_durasi_start" value="<?php echo date('d/m/Y')?>" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group" id="data_1">
                                        <label>Promo End</label>
                                        <div class="input-group date">
                                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                            <input type="text" class="form-control" placeholder="Durasi End" name="tgl_durasi_end" value="<?php echo date('d/m/Y')?>" required="">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Biaya Fixed<small>(Rp.)</small></label>
                                        <input type="text" class="form-control" placeholder="Fixed Cost" name="biaya_fixed" required="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>Biaya Variabel<small>(Rp.)</small></label>
                                            <input type="text" class="form-control" placeholder="Variable Cost" name="biaya_variable"  required="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>GP<small>(%)</small></label>
                                            <input type="text" class="form-control" placeholder="Gross Profit" name="gp" required="">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label>Target Budget Tahunan<small>(Rp.)</small></label>
                                            <input type="text" class="form-control" placeholder="Annual Budget Target" name="target_budget_tahunan" required="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                            <label>Budget Terpakai<small>(Rp.)</small></label>
                                            <input type="text" class="form-control" placeholder="Budget Used" name="budget_terpakai"  required="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                            <label>Total Budget Terpakai<small>(Rp.)</small></label>
                                            <input type="text" class="form-control" placeholder="Tottal Used Budget" name="total_budget_terpakai"  required="">
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                            <label>Margin<small>(Rp.)</small></label>
                                            <input type="text" class="form-control" placeholder="Margins" name="margin" required="">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                            <input type="submit" class="btn btn-sm btn-primary" value="SIMPAN">
                                            <a href="<?php echo base_url('promo') ?>" class="btn btn-sm btn-info">KEMBALI</a>
                                            <input type="reset" value="RESET" class="btn btn-sm btn-warning" />
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
    $('#data_1 .input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,

            format:'dd/mm/yyyy'



        });
    $("#inputmasking1, #inputmasking2, #inputmasking3, #inputmasking4, #inputmasking5, #inputmasking6, #inputmasking7, #inputmasking8").mask("000.000.000.000", {reverse: true});
    // $(document).on("keyup", "#inputmasking1, #inputmasking2, #inputmasking3, #inputmasking4, #inputmasking5, #inputmasking6, #inputmasking7, #inputmasking8", function(e){$(this).val("Rp." + $(this).val());
    });
// var rupiah = document.getElementById("rupiah");
// rupiah.addEventListener("keyup", function(e) {
//   // tambahkan 'Rp.' pada saat form di ketik
//   // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
//   rupiah.value = formatRupiah(this.value, "Rp. ");
// });

// /* Fungsi formatRupiah */
// function formatRupiah(angka, prefix) {
//   var number_string = angka.replace(/[^,\d]/g, "").toString(),
//     split = number_string.split(","),
//     sisa = split[0].length % 3,
//     rupiah = split[0].substr(0, sisa),
//     ribuan = split[0].substr(sisa).match(/\d{3}/gi);

//   // tambahkan titik jika yang di input sudah menjadi angka ribuan
//   if (ribuan) {
//     separator = sisa ? "." : "";
//     rupiah += separator + ribuan.join(".");
//   }

//   rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
//   return prefix == undefined ? rupiah : rupiah ? "Rp. " + rupiah : "";
// }

    $("#percentmasking").mask("##0,00", {reverse: true});
</script>