<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_location extends CI_Model {

    public function getProvince(){
        $this->db->select("*");
		$this->db->from("province");
		$this->db->order_by("province", "asc");
        
        return $this->db->get()->result();
    }
}