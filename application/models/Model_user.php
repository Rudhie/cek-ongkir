<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {

    public function getUser($id = null){
        if(isset($id)){
            $this->db->where("id_user", $id);
        }
        $this->db->select("id_user, nama_user, email, password, role, status");
		$this->db->from("tbl_user");
		$this->db->order_by("id_user", "desc");
        
        return $this->db->get()->result();
    }

    public function tambahUser($data){
        $this->form_validation->set_rules("email","email", "is_unique[tbl_user.email]");
        if($this->form_validation->run() != false){     
        $this->db->insert("tbl_user", $data);
        return true;
        } else {
            return false;
        }
    }
    
    public function ubahUser($data, $idUser, $email_baru){
        $email_lama = $this->db->query("SELECT email FROM tbl_user WHERE id_user = ".$idUser)->row()->email;
        if($email_baru != $email_lama) {
            $is_unique =  'is_unique[tbl_user.email]';
        } else {
            $is_unique =  '';
        }

        if($is_unique==''){
            $this->db->where("id_user", $idUser)->update("tbl_user", $data);
            return true;
        }else{
            $this->form_validation->set_rules("email","email", $is_unique);
            if($this->form_validation->run() != false){
                $this->db->where("id_user", $idUser)->update("tbl_user", $data);
                return true;
            }else {
                return false;
            }
        }    
    }
    public function ubahProfil($data, $idUser){
        $this->db->where("id_user", $idUser)->update("tbl_user", $data);
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
    
    public function hapusUser($idUser){
        $this->db->where("id_user", $idUser)->delete("tbl_user");
        if($this->db->affected_rows() > 0){
            return true;
        } else {
            return false;
        }
    }
}
?>