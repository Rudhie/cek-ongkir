<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landingpage extends CI_Controller {

    public function __construct(){
		parent::__construct();
		$this->load->model('Model_location');
        // checkSessionUser();
    }

    public function index(){
    	$data['province'] = $this->Model_location->getProvince();
        $this->load->view('landing-page', $data);
    }
}