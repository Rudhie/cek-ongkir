<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Promo extends CI_Controller {

	private $filename = "import_data";

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        $this->load->model("Model_promo");
    }

    public function index(){
		$data["promo"] = $this->Model_promo->getPromo();
		$this->template->load("template", "promo/data-promo", $data);
    }
    
    public function tambah(){
		$this->template->load("template", "promo/form-promo");
    }
    
     public function edit(){
        $id = $this->input->get("id");
		$data["promo"] = $this->Model_promo->getPromo($id);
		$this->template->load("template", "promo/edit-promo", $data);
    }
    public function detail(){
		$id = $this->input->get("id");
		$data["promo"] = $this->Model_promo->getPromo("tbl_promo.id_promo", $id);
		$this->template->load("template", "promo/detail-promo", $data);
	}

    public function action_tambah(){
		$nama_akun = $this->input->post("nama_akun");
		$nama_promo = $this->input->post("nama_promo");
		$tgl_sell_out_total = $this->input->post("tgl_sell_out_total");
		$sku_total = $this->input->post("sku_total");
		$quantity_total = $this->input->post("quantity_total");
		$price_total = $this->input->post("price_total");
		$tgl_sell_out_onpromo = $this->input->post("tgl_sell_out_onpromo");
		$sku = $this->input->post("sku");
		$quantity = $this->input->post("quantity");
		$price = $this->input->post("price");
		$tgl_durasi_start = $this->input->post("tgl_durasi_start");
		$tgl_durasi_end = $this->input->post("tgl_durasi_end");
		$biaya_fixed = $this->input->post("biaya_fixed");
		$biaya_variable = $this->input->post("biaya_variable");
		$gp = $this->input->post("gp");
		$target_budget_tahunan = $this->input->post("target_budget_tahunan");
		$budget_terpakai = $this->input->post("budget_terpakai");
		$total_budget_terpakai = $this->input->post("total_budget_terpakai");
		$margin = $this->input->post("margin");


		$dataPromo = array(
		"nama_akun" => $nama_akun,
		"nama_promo" => $nama_promo,
		"tgl_sell_out_total" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_sell_out_total))),
		"sku" => $sku,
		"quantity" => $quantity,
		"price" => $price,
		"tgl_sell_out_onpromo" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_sell_out_onpromo))),
		"sku_total" => $sku_total,
		"quantity_total" => $quantity_total,
		"price_total" => $price_total,
		"tgl_durasi_start" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_durasi_start))),
		"tgl_durasi_end" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_durasi_end))),
		"biaya_fixed" => $biaya_fixed,
		"biaya_variable" => $biaya_variable,
		"gp" => $gp,
		"target_budget_tahunan" => $target_budget_tahunan,
		"budget_terpakai" => $budget_terpakai,
		"total_budget_terpakai" => $total_budget_terpakai,
		"margin" => $margin

		);

		$tambahPromo = $this->Model_promo->tambahPromo($dataPromo);
		if($tambahPromo){
			$this->session->set_flashdata("success", "BERHASIL MENYIMPAN DATA PROMO");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENYIMPAN DATA PROMO");
		}

		redirect("promo");
	}

	public function action_ubah($idPromo){
		$nama_akun = $this->input->post("nama_akun");
		$nama_promo = $this->input->post("nama_promo");
		$tgl_sell_out_total = $this->input->post("tgl_sell_out_total");
		$sku_total = $this->input->post("sku_total");
		$quantity_total = $this->input->post("quantity_total");
		$price_total = $this->input->post("price_total");
		$tgl_sell_out_onpromo = $this->input->post("tgl_sell_out_onpromo");
		$sku = $this->input->post("sku");
		$quantity = $this->input->post("quantity");
		$price = $this->input->post("price");
		$tgl_durasi_start = $this->input->post("tgl_durasi_start");
		$tgl_durasi_end = $this->input->post("tgl_durasi_end");
		$biaya_fixed = $this->input->post("biaya_fixed");
		$biaya_variable = $this->input->post("biaya_variable");
		$gp = $this->input->post("gp");
		$target_budget_tahunan = $this->input->post("target_budget_tahunan");
		$budget_terpakai = $this->input->post("budget_terpakai");
		$total_budget_terpakai = $this->input->post("total_budget_terpakai");
		$margin = $this->input->post("margin");
		

		$dataPromo = array(
		"nama_akun" => $nama_akun,
		"nama_promo" => $nama_promo,
		"tgl_sell_out_total" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_sell_out_total))),
		"sku" => $sku,
		"quantity" => $quantity,
		"price" => $price,
		"tgl_sell_out_onpromo" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_sell_out_onpromo))),
		"sku_total" => $sku_total,
		"quantity_total" => $quantity_total,
		"price_total" => $price_total,
		"tgl_durasi_start" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_durasi_start))),
		"tgl_durasi_end" => date("Y-m-d", strtotime(str_replace("/", "-", $tgl_durasi_end))),
		"biaya_fixed" => $biaya_fixed,
		"biaya_variable" => $biaya_variable,
		"gp" => $gp,
		"target_budget_tahunan" => $target_budget_tahunan,
		"budget_terpakai" => $budget_terpakai,
		"total_budget_terpakai" => $total_budget_terpakai,
		"margin" => $margin

		);

		$ubahPromo = $this->Model_promo->ubahPromo($dataPromo, $idPromo);
		if($ubahPromo){
			$this->session->set_flashdata("success", "BERHASIL MENGUBAH DATA PROMO");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGUBAH DATA PROMO");
		}

		

		redirect("promo");

	}

	public function action_hapus($idPromo){
		$hapusPromo = $this->Model_promo->hapusPromo($idPromo);
		if($hapusPromo){
			$this->session->set_flashdata("success", "BERHASIL MENGHAPUS DATA PROMO");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGHAPUS DATA PROMO");
		}

		redirect("promo");
	}

	 // public function tambah1(){
		// $this->template->load("template", "promo/import");
  //   }

	public function import(){
		$data = array(); // Buat variabel $data sebagai array
		
		if(isset($_POST['preview'])){ // Jika user menekan tombol Preview pada form
			// lakukan upload file dengan memanggil function upload yang ada di SiswaModel.php
			$upload = $this->Model_promo->upload_file($this->filename);
			// exit();
			if($upload['result'] == "success"){ // Jika proses upload sukses
				// Load plugin PHPExcel nya
				include APPPATH.'third_party/PHPExcel/PHPExcel.php';
				
				$excelreader = new PHPExcel_Reader_Excel2007();
				$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang tadi diupload ke folder excel
				$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
				
				// Masukan variabel $sheet ke dalam array data yang nantinya akan di kirim ke file form.php
				// Variabel $sheet tersebut berisi data-data yang sudah diinput di dalam excel yang sudha di upload sebelumnya
				$data['sheet'] = $sheet; 
			}else{ // Jika proses upload gagal
				$data['upload_error'] = $upload['error']; // Ambil pesan error uploadnya untuk dikirim ke file form dan ditampilkan
			}
		}
		
		// $this->load->view('promo/import', $data);
		$this->template->load("template", "promo/import", $data);

		
	}

		public function import_data(){
		// Load plugin PHPExcel nya
		include APPPATH.'third_party/PHPExcel/PHPExcel.php';
		
		$excelreader = new PHPExcel_Reader_Excel2007();
		$loadexcel = $excelreader->load('excel/'.$this->filename.'.xlsx'); // Load file yang telah diupload ke folder excel
		$sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);
		
		// Buat sebuah variabel array untuk menampung array data yg akan kita insert ke database
		$data = array();
		
		$numrow = 1;
		foreach($sheet as $row){
			// Cek $numrow apakah lebih dari 1
			// Artinya karena baris pertama adalah nama-nama kolom
			// Jadi dilewat saja, tidak usah diimport
			if($numrow > 1){
				// Kita push (add) array data ke variabel data
				array_push($data, array(
					 'nama_akun'=>$row['A'], // Insert data nis dari kolom A di excel
					'nama_promo'=>$row['B'], // Insert data nama dari kolom B di excel
					'tgl_durasi_start'=>$row['C'], // Insert data jenis kelamin dari kolom C di excel
					'tgl_durasi_end'=>$row['D'], // Insert data alamat dari kolom D di excel
					'tgl_sell_out_total'=>$row['E'],
					'sku_total'=>$row['F'],
					'quantity_total'=>$row['G'],
					'price_total'=>$row['H'],
					'tgl_sell_out_onpromo'=>$row['I'],
					'sku'=>$row['J'],
					'quantity'=>$row['K'],
					'price'=>$row['L'],
					'biaya_fixed'=>$row['M'],
					'biaya_variable'=>$row['N'],
					'gp'=>$row['O'],
					'target_budget_tahunan'=>$row['P'],
					'budget_terpakai'=>$row['Q'],
					'total_budget_terpakai'=>$row['R'],
					'margin'=>$row['S'],
				));
			}
			
			$numrow++; // Tambah 1 setiap kali looping
		}

		// Panggil fungsi insert_multiple yg telah kita buat sebelumnya di model
		$this->Model_promo->insert_multiple($data);
		
		redirect('Promo'); // Redirect ke halaman awal (ke controller siswa fungsi index)
	}
}
?>