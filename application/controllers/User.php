<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct(){
		parent::__construct();
        checkSessionUser();
        $this->load->model("Model_user");
    }

    public function index(){
		$data["user"] = $this->Model_user->getUser();
		$this->template->load("template", "user/data-user", $data);
    }
    
    public function tambah(){
		$this->template->load("template", "user/form-user");
    }
    
     public function edit(){
        $id = $this->input->get("id");
		$data["user"] = $this->Model_user->getUser($id);
		$this->template->load("template", "user/edit-user", $data);
    }
	public function edit_account() {
		$id=$this->input->post("id");
		$e['nama_user'] = $this->input->post('nama_user');
		$e['email'] = $this->input->post('email');
		$e['password'] = $this->input->post('password');
		$e['role'] = $this->input->post('role');
		$e['status'] = $this->input->post('status');

		$update = $this->db->query("UPDATE tbl_user SET
									nama_user = '$e[nama_user]',
									email = '$e[email]',
									password = '$e[password]',
									role = '$e[role]',
									status = '$e[status]'
								WHERE id_user='$id'");
		if($update) {
		$this->session->set_userdata("nama_user",$e['nama_user']);
			redirect('user');
		}
		else {
			redirect('user');
		}
	}

    public function action_tambah(){
		$nama_user = $this->input->post("nama_user");
		$email = $this->input->post("email");
		$password = $this->input->post("password");
        $role = $this->input->post("role");
        $status = $this->input->post("status");
        
		$dataUser = array(
			"nama_user" => $nama_user,
			"email" => $email,
			"password" => md5($password),
			"role" => $role,
			"status" => $status

		);

		$tambahUser = $this->Model_user->tambahUser($dataUser, $idUser);
		if($tambahUser){
			$this->session->set_flashdata("success", "BERHASIL MENYIMPAN DATA USER");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENYIMPAN DATA USER");
		}

		redirect("user");
	}

	public function action_ubah($idUser){
		$nama_user = $this->input->post("nama_user");
		$email = $this->input->post("email");
		$password = $this->input->post("password");
        $role = $this->input->post("role");
        $status = $this->input->post("status");
		
		$dataUser = array(
			"nama_user" => $nama_user,
			"email" => $email,
			"password" => md5($password),
			"role" => $role,
			"status" => $status
		);

		$ubahUser = $this->Model_user->ubahUser($dataUser, $idUser, $email);
		if($ubahUser){
			$this->session->set_flashdata("success", "BERHASIL MENGUBAH DATA USER");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGUBAH DATA USER");
		}

		redirect("user");

	}

	public function action_hapus($idUser){
		$hapusUser = $this->Model_user->hapusUser($idUser);
		if($hapusUser){
			$this->session->set_flashdata("success", "BERHASIL MENGHAPUS DATA USER");
		} else {
			$this->session->set_flashdata("error", "GAGAL MENGHAPUS DATA USER");
		}

		redirect("user");
	}
}
?>